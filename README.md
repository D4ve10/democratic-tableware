# Democratic Tableware

### Getting started

1. [node js](https://nodejs.org/dist/v12.8.0/node-v12.8.0-x64.msi) installieren

2. [GitHub Desktop](https://desktop.github.com/) installieren

3. [Visual Studio Code](https://code.visualstudio.com/) installieren

4. Entweder
   ```bash
   git clone https://D4ve10@bitbucket.org/D4ve10/democratic-tableware.git
   ```
   Oder GitHub Desktop öffnen und dann folgendes auswählen: File -> Clone Repository
   
5. Zum Bearbeiten der Dateien, Visual Studio Code öffnen und den Ordner auswählen

6. Zum Installieren aller Pakete, Windows Eingabeaufforderung öffnen und folgende Befehle ausführen:

   ```bash
   cd democratic-tableware
   npm install -g expo-cli
   npm install
   ```

7. Jetzt kann die App gestartet werden

   ```bash
   expo start
   ```
   
8. Um die App auf dem Handy zu simulieren, muss man nur noch "expo" aus dem Google Play Store oder App Store herunterladen und den QR-Code aus dem vorherigen Befehl einscannen.
   Falls es Probleme mit dem Verbinden gibt, benutzt bitte diesen Befehl:
   ```bash
   expo start --tunnel
   ```
   
   

