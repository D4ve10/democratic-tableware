import React, { Component } from "react";
import { View, Text, Modal, StyleSheet, TextInput, TouchableOpacity, Alert } from "react-native";
// import { withSafeArea } from 'react-native-safe-area'
const SaveAreaView = View

import CloseButton from "./CloseButton"
import Firebase from "./../../js/Firebase"
import createUniqueID from "./../../js/createUniqueID"

export default class QuestionModal extends Component {

  state = {
    question: ""
  }

  _onSubmit() {
    if (!this.state.question)
      return
    Alert.alert(
      "Do you want to create this voting?",
      this.state.question,
      [
        { text: "cancel", style: "cancel" },
        { text: "OK", onPress: () => { this._createVoting() } }
      ],
      { cancelable: true }
    )
  }

  _createVoting() {
    Firebase.db.ref(`/sessions/${Firebase.sessionID}/votes`).once("value", snapshot => {
      let voteIDs = Object.keys(snapshot.val() || {})
      let uniqueID = createUniqueID(voteIDs)
      Firebase.db.ref(`/sessions/${Firebase.sessionID}/votes/${uniqueID}`).set({
        id: uniqueID,
        completed: false,
        question: this.state.question,
        time: 60
      })
      this.setState({ question: "" })
      this.props.onCreated(uniqueID)
    })
  }

  render() {
    return (
      <SaveAreaView>
        <Modal
          animationType="slide"
          visible={this.props.visible}
          onRequestClose={this.props.onRequestClose}
        >
          <View style={{flex: 1, backgroundColor: "#303030"}}>
          <CloseButton
            style={{margin: 7}}
            onPress={this.props.onRequestClose}
          />
          <Text style={styles.text}>
            Type in the Question you want to vote on
          </Text>
          <TouchableOpacity
            style={styles.inputView}
            activeOpacity={1}
            onPress={() => {
              if (this.questionInput) {
                this.questionInput.blur()
                setTimeout(() => {this.questionInput.focus()}, 100)
              }
            }}
          >
            <TextInput
              ref={input => { this.questionInput = input }}
              style={styles.questionInput}
              placeholder="..."
              onChangeText={text => { this.setState({ question: text }) }}
              value={this.state.question}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => { this._onSubmit() }}
          >
            <Text style={[styles.text, { fontSize: 18, fontWeight: "bold" }]}>
              Submit
            </Text>
          </TouchableOpacity>
          </View>
        </Modal>
      </ SaveAreaView>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    textAlign: "center",
    marginTop: 20,
    color: "#fff"
  },
  inputView: {
    height: "25%",
    width: "75%",
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 20,
    alignSelf: "center"
  },
  questionInput: {
    marginTop: "25%",
    textAlign: "center",
    color: "#fff"
  }
})