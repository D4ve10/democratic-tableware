import React, { Component } from "react";
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, TouchableOpacity } from "react-native";

export default class CloseButton extends Component {

  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        style={[styles.closeButton, this.props.style]}
      >
        <Ionicons
          name="md-close"
          size={32}
          style={{color: "#fff"}}
        />
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  closeButton: {
    left: 10,
    right: 0,
    top: 0,
    bottom: 0
  }
})