import React, { Component } from 'react';
import { Image, StyleSheet, View, Dimensions, Text, Slider } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

const gradientWidth = Dimensions.get("window").width / 3
const gradientHeight = Dimensions.get("window").height * 0.8
const offset = Dimensions.get("window").width / 2 - Dimensions.get("window").height / 2

export default class VoteSlider extends Component {
  render() {
    return (
      <View style={styles.colorImage}>
        <LinearGradient
          colors={['green', 'yellow', "red"]}
          style={styles.gradient}
        >
        <View style={styles.slider}>
          <Slider
            minimumValue={-100}
            maximumValue={100}
            step={1}
            style={{alignSelf: "center", transform: [{scale: 3}, {rotate: "-90deg"}], width: gradientHeight / 3}}
            value={0}
            thumbTintColor={"white"}
            minimumTrackTintColor={"transparent"}
            maximumTrackTintColor={"transparent"}
            onSlidingComplete={value => { this.props.onSlidingComplete(value) }}
          />
        </View>
        </LinearGradient>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  colorImage: {
    flex: 1
  },
  gradient: {
    alignSelf: "center",
    height: gradientHeight,
    width: gradientWidth,
    borderRadius: gradientWidth / 2
  },
  slider: {
    flex: 1,
    transform: [
      // { translateX: -offset },
      // { translateY: offset }
      {scale: 1}
    ],
    // backgroundColor: "red",
    marginLeft: 0,
    justifyContent: "center",
    alignSelf: "center",
  }
})
