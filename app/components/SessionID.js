import React, { Component } from "react";
import { StyleSheet, Text } from "react-native";

import Firebase from "./../../js/Firebase";

export default class SessionID extends Component {

  render() {
    return (
      <Text style={styles.sessionID}>
        ID: {Firebase.sessionID}
      </Text>
    )
  }
}

const styles = StyleSheet.create({
  sessionID: {
    left: 10,
    top: 5,
    position: "absolute",
    color: "#fff"
  }
})