import React, { Component } from "react";
import { View, Text, StyleSheet, FlatList, SafeAreaView, Dimensions } from "react-native";
import { useNavigation } from '@react-navigation/native';
import Firebase from "../../js/Firebase";

import VoteItem from "./../components/VoteItem"

class VoteList extends Component {

  state = {
    votes: []
  }

  async componentDidMount() {
    if (!Firebase.sessionID)
      return
    Firebase.db.ref(`/sessions/${Firebase.sessionID}/votes`).on("value", snapshot => {
      let votes = Object.values(snapshot.val() || {})
      this.setState({ votes })
    })
    }
  async componentWillUnmount() {
    Firebase.db.ref(`/sessions/${Firebase.sessionID}/votes`).off()
  }
  _onPressVote(vote) {
    if (vote.time > 0 && !Object.keys(vote.users || {}).includes(Firebase.auth.currentUser.uid)) {
      this.props.navigation.navigate("Voting", { voteID: vote.id })
    }else {
      this.props.navigation.navigate("AdminEvaluate", { voteID: vote.id })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.conversations}>
          Your conversations
        </Text>
        <SafeAreaView style={styles.voteList}>
        <FlatList
          renderItem={({ item }) => (
            <VoteItem item={item} onPress={() => this._onPressVote(item)}/>
          )}
          data={this.state.votes}
          keyExtractor={(item, index) => item + index}
          showsVerticalScrollIndicator={false}
        />
        </SafeAreaView>
      </View>
    )
  }
}
export default function(props) {
  const navigation = useNavigation()
  return <VoteList {...props} navigation={navigation} />
}

const styles = StyleSheet.create({
  conversations: {
    fontWeight: "bold",
    marginLeft: 20, 
    alignSelf: "flex-start",
    color: "#fff"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  voteList: {
    flex: 1,
    alignSelf: "flex-end",
    justifyContent: "flex-end",
    marginBottom: 10
  }
})