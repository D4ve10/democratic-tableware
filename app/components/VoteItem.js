import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import {Dimensions } from "react-native";


export default class CloseButton extends Component {

  render() {
    return (
      <TouchableOpacity
        style={styles.item}
        onPress={this.props.onPress}  
      >
        <Text style={styles.question}>
        {this.props.item.question}
        </Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  item: {
  borderWidth: 1,
  borderColor: "white",
  borderRadius: 15,
  width: Math.round(Dimensions.get('window').width) - 50,
  height: 100,
  marginTop: 20,
  justifyContent: "center"
  },
  question: {
  fontSize: 20,
  color: "#fff",
  alignSelf: "center"
  }
})