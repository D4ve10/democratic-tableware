import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Alert } from "react-native";

import VoteSlider from "./../components/VoteSlider"
import Firebase from "./../../js/Firebase"

export default class VotingScreen extends Component {

  state = {
    value: 0,
    vote: null
  }

  async componentDidMount() {
    if (!this.props.route.params.voteID || !Firebase.sessionID)
      return
    Firebase.db.ref(`/sessions/${Firebase.sessionID}/votes/${this.props.route.params.voteID}`).on("value", snapshot => {
      let vote = snapshot.val()
      if (!vote)
        return this.props.navigation.goBack()
      this.setState({ vote })
    })
  }

  async componentWillUnmount() {
    if (!this.props.route.params.voteID || !Firebase.sessionID)
      return
    Firebase.db.ref(`/sessions/${Firebase.sessionID}/votes/${this.props.route.params.voteID}`).off()
  }

  _onSubmit() {
    Alert.alert(
      "Are you sure?",
      this.state.vote.question,
      [
        { text: "cancel", style: "cancel" },
        { text: "YES", onPress: () => { this._makeVoting() } }
      ],
      { cancelable: true }
    )
  }

  _makeVoting() {
    Firebase.db.ref(`/sessions/${Firebase.sessionID}/votes/${this.state.vote.id}/users/${Firebase.auth.currentUser.uid}`)
      .set(this.state.value)
    this.props.navigation.replace("AdminEvaluate", {voteID: this.state.vote.id})
  }

  render() {
    if (!this.state.vote)
      return null
    return (
      <View style={{flex: 1}}>
        {/* <Text style={styles.text}>
          The admin wants to vote
        </Text>
        <View style={styles.questionView}>
          <Text style={styles.questionText}>
            {this.state.vote.question}
          </Text>
        </View> */}
      <View style={styles.slider}>
        <VoteSlider
        onSlidingComplete={value => { this.setState({ value }) }}
        />
      </View>
      <TouchableOpacity
        style={{ flex: 1, position: "absolute", bottom: 0, right: 0, margin: 15}}
        onPress={() => { this._onSubmit() }}
      >
        <Text style={[styles.text, { fontSize: 18, fontWeight: "bold" }]}>
          Vote
        </Text>
      </TouchableOpacity>
      </ View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    textAlign: "center",
    marginTop: 20,
    color: "#fff"
  },
  questionView: {
    flex: 1,
    height: 20,
    width: 200,
    marginTop: 20,
    alignSelf: "center"
  },
  questionText: {
    padding: 5,
    borderColor: 'gray',
    borderWidth: 1,
    color: "#fff"
  },
  slider: {
    flex: 1,
    marginTop: 20,
    justifyContent: "center"
  },
  questionInput: {
    marginTop: "25%",
    textAlign: "center",
    color: "#fff"
  }
})