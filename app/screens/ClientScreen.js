import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';

import Firebase from '../../js/Firebase';

export default class ClientScreen extends Component {
  state = {
    sessionID: null
  }

  async componentDidMount() {
  }

  _joinSession() {
    this.setState({ error: null })
    Firebase.db.ref("/sessions").once("value", snapshot => {
      let sessions = snapshot.val()
      if (sessions && typeof sessions === "object") {
        for (let session of Object.values(sessions)) {
          if (!session.completed /* && session.owner !== Firebase.auth.currentUser.uid*/) {
            if (this.state.sessionID === session.id.toString()) {
              Firebase.setSessionID(this.state.sessionID)
              return this.props.navigation.navigate("UserSession")
            }
          }
        }
      }
      this.setState({ error: "Couldn't find that session" })
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.center}
          placeholder="Session ID"
          keyboardType="number-pad"
          onChangeText={text => { this.setState({ sessionID: text.replace(/[^0-9]/g, '') }) }}
          value={this.state.sessionID}
        />
        <TouchableOpacity
          onPress={() => this._joinSession()}
        >
          <Text style={[styles.center, styles.submit]}>
            Submit
          </Text>
        </TouchableOpacity>
        <Text style={[styles.center, { color: "red" }]}>
          {this.state.error}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  center: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: "#fff"
  },
  submit: {

  }
});
