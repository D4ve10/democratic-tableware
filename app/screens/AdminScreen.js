import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import createUniqueID from "../../js/createUniqueID"
import Firebase from '../../js/Firebase'
import SessionID from '../components/SessionID'
import QuestionModal from '../components/QuestionModal';
import VoteList from '../components/VoteList';

export default class AdminScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerRight: () =>
      <TouchableOpacity
        onPress={() => navigation.navigate("AdminSettingsScreen")}
      >
        <Ionicons name="md-settings" size={28} style={{ marginRight: 15}}/>
      </TouchableOpacity>
  })

  state = {
    sliderValue: 0,
    sessionID: null,
    creatingVote: false
  }

  async componentDidMount() {
    Firebase.db.ref("/sessions").once("value", snapshot => {
      var sessions = snapshot.val()
      if (sessions && typeof sessions === "object") {
        for (let session of Object.values(sessions)) {
          if (session.owner === Firebase.auth.currentUser.uid && !session.completed) {
            Firebase.setSessionID(session.id);
            return this.setState({ sessionID: session.id })
          }
        }
      }
      if (!sessions) {
        sessions = {}
      }
      let sessionIDs = Object.keys(sessions)
      let uniqueID = createUniqueID(sessionIDs);
      Firebase.db.ref(`/sessions/${uniqueID}`).set({
        id: uniqueID,
        owner: Firebase.auth.currentUser.uid,
        completed: false,
      })
      Firebase.setSessionID(uniqueID);
      this.setState({ sessionID: uniqueID })
    })
  }

  _createVoting() {
    this.setState({ creatingVote: true })
  }

  _createdVoting(voteID) {
    this.setState({ creatingVote: false });
    this.props.navigation.navigate("Voting", { voteID })
  }

  render() {
    if (typeof this.state.sessionID !== "number")
      return (
        <View style={styles.container}>
          <Text style={styles.paragraph}>
            Loading ...
          </Text>
        </View>
      )
    return (
      <View style={styles.container}>
        <SessionID />
        <TouchableOpacity
          style={{marginBottom: 30}}
          onPress={() => { this._createVoting() }}
        >
          <Text style={styles.paragraph}>
            Create Voting
          </Text>
        </TouchableOpacity>
        <VoteList />
        <QuestionModal
          visible={this.state.creatingVote}
          onRequestClose={() => { this.setState({ creatingVote: false }) }}
          onCreated={voteID => { this._createdVoting(voteID) }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  paragraph: {
    margin: 12,
    fontSize: 18,
    fontWeight: 'bold',
    color: "#fff",
    textAlign: 'center',
  },
  sessionID: {
    left: 0,
    top: 0,
    position: "absolute"
  }
});
