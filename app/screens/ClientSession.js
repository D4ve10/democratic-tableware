import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import Firebase from '../../js/Firebase';
import VoteList from '../components/VoteList';
import SessionID from '../components/SessionID';

export default class ClientSession extends Component {
  state = {
    vote: null
  }

  // async componentDidMount() {
  //   if (!Firebase.sessionID)
  //     return
  //   Firebase.db.ref(`/sessions/${Firebase.sessionID}/votes`).on("value", snapshot => {
  //     let votes = Object.values(snapshot.val() || {})
  //     let availableVotes = votes.filter(value => value.time > 0 && value.completed === false
  //       && !Object.keys(value.users || {}).includes(Firebase.auth.currentUser.uid))
  //     if (availableVotes.length > 0) {
  //       this.setState({ votesChecked: true, vote: availableVotes[0] })
  //     }
  //     this.setState({ votesChecked: true })
  //   })
  // }
  // async componentWillUnmount() {
  //   Firebase.db.ref(`/sessions/${Firebase.sessionID}/votes`).off()
  // }

  render() {
    return (
      <View style={styles.container}>
        <SessionID />
        <View style={styles.voteList}>
          <VoteList />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  voteList: {
    paddingTop: 50
  }
});
