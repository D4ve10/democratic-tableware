import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import { PieChart } from "react-native-chart-kit";

import Firebase from '../../js/Firebase'

export default class AdminEvalScreen extends Component {

  state = {
    vote: null
  }

  async componentDidMount() {
    if (!this.props.route.params.voteID || !Firebase.sessionID)
      return
    Firebase.db.ref(`/sessions/${Firebase.sessionID}/votes/${this.props.route.params.voteID}`).on("value", snapshot => {
      let vote = snapshot.val()
      if (!vote)
        return this.props.navigation.goBack()
      let userVotes = Object.values(vote.users || {})
      if (userVotes.length === 0)
        return
      userVotes = userVotes.map(number => this._convertToPercentage(number))
      let proVote = userVotes.reduce((a, b) => a + b, 0) / userVotes.length
      this.setState({ vote: { ...vote, ...{ proVote } } })
    })
  }

  _convertToPercentage(number) {
    return (number + 100) / 2
  }

  async componentWillUnmount() {
    if (!this.props.route.params.voteID || !Firebase.sessionID)
      return
    Firebase.db.ref(`/sessions/${Firebase.sessionID}/votes/${this.props.route.params.voteID}`).off()
  }

  render() {
    if (!this.state.vote || (!this.state.vote.proVote && this.state.vote.proVote !== 0))
      return null
    return (
      <View>
        <Text style={styles.text}>
          Question: {this.state.vote.question}
        </Text>
        <PieChart
          data={
            [
              {
                name: "Pro",
                vote: this.state.vote.proVote,
                color: "rgb(189, 215, 238)",
                legendFontColor: "#fff"
              },
              {
                name: "Kontra",
                vote: 100 - this.state.vote.proVote,
                color: "rgb(173, 185, 202)",
                legendFontColor: "#fff"
              }
            ]
          }
          width={Dimensions.get("window").width}
          height={220}
          chartConfig={chartConfig}
          accessor="vote"
          backgroundColor="transparent"
          paddingLeft="15"
          style={{ top: "50%" }}
          absolute
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    textAlign: "center",
    marginTop: 20,
    color: "#fff"
  },
});

const chartConfig = {
  backgroundGradientFrom: "#1E2923",
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: "#08130D",
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
  strokeWidth: 2, // optional, default 3
  barPercentage: 1
};
