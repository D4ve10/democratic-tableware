import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import ClientScreen from './app/screens/ClientScreen'
import ClientSession from './app/screens/ClientSession'
import AdminScreen from './app/screens/AdminScreen'
import AdminEvalScreen from './app/screens/AdminEvalScreen'
import VotingScreen from './app/screens/VotingScreen'
import Firebase from './js/Firebase'

import { StyleSheet, Text, View, YellowBox, TouchableOpacity, Image, Dimensions} from 'react-native';

// ignore annoying warnings
YellowBox.ignoreWarnings([
  'Setting a timer'
]);
console.ignoredYellowBox = ["Setting a timer"]

class App extends Component {

  state = {
    isAuthenticated: false
  }

  async componentDidMount() {
    Firebase.init()
    Firebase.auth.setPersistence("local")
      .then(() => {
        return Firebase.auth.signInAnonymously()
          .then(() => {
            this.setState({ isAuthenticated: true })
          })
      })
  }

  render() {
    if (!this.state.isAuthenticated)
      return (
        <View style={styles.container}>
          <Text style={styles.paragraph}>Loading ...</Text>
        </View>
      );
    return (
      <View style={styles.container}>
        <Text style={[styles.paragraph, {marginTop: 20}]}>
          Welcome!
        </Text>
        <Image
          source={require("./assets/DtLogo.png")}
          style={{ width: Dimensions.get("screen").width * 0.8, height: Dimensions.get("screen").height * 0.55, alignSelf: "center" }}
        />
        <TouchableOpacity
          onPress={() => { this.props.navigation.navigate("UserLogin") }}
        >
          <Text style={styles.paragraph}>
            Guest
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => { this.props.navigation.navigate("Admin") }}
        >
          <Text style={styles.paragraph}>
            Admin
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const MyTheme = {
  dark: false,
  colors: {
    primary: '#fff',
    background: '#303030',
    card: '#303030',
    text: '#fff',
    border: '#fff',
  },
}

const Stack = createStackNavigator();

export default function() {
  return (
    <NavigationContainer theme={MyTheme}>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={App} options={{headerShown: false }}/>
        <Stack.Screen name="UserLogin" component={ClientScreen}/>
        <Stack.Screen name="UserSession" component={ClientSession}/>
        <Stack.Screen name="Admin" component={AdminScreen} />
        <Stack.Screen name="AdminEvaluate" component={AdminEvalScreen}/>
        <Stack.Screen name="Voting" component={VotingScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: 'center',
    fontWeight: "bold",
    color: "#fff"
  },
});
