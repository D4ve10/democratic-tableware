export default sessionIDs => {
  let sessionID = Math.floor(Math.random() * 900000) + 100000
  while (sessionIDs.includes(sessionID)) {
    sessionID = Math.floor(Math.random() * 900000) + 100000
  }
  return sessionID
}