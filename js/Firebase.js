import firebase from 'firebase';
import 'firebase/database';

const config = {
  apiKey: "AIzaSyCTETyEY0p8cb-sU-JrV66TwYxeELqy5Ic",
  authDomain: "informatik-ck.firebaseapp.com",
  databaseURL: "https://informatik-ck.firebaseio.com",
  projectId: "informatik-ck",
  storageBucket: "informatik-ck.appspot.com",
  messagingSenderId: "308855553226",
  appId: "1:308855553226:web:653bd002254573657a0833"
};
export default class Firebase {
  static db;
  static sessionID;

  static init() {
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    }
    Firebase.db = firebase.database();
    Firebase.auth = firebase.auth();
  }

  static setSessionID(sessionID) {
    Firebase.sessionID = sessionID
  }
}